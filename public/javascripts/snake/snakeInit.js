
$(document).ready(function () {
	var map   = snakeMap;
	var snake = new SnakeAbstraction(map);
	var ui    = new SnakeUI(map);
	
	snake.setMoveTime(200);
	snake.setFoodTime(18000);
	

	ui.on('left' , snake.toWest);
	ui.on('right', snake.toEast);
	ui.on('up'   , snake.toNorth);
	ui.on('down' , snake.toSouth);
	
	ui.on('r'    , snake.start);

	snake.on('move'    , ui.drawSnake);
	snake.on('gameOver', ui.gameOver);
	snake.on('foodGenerated', ui.drawFood);
	snake.on('eat'     , ui.eat);
});


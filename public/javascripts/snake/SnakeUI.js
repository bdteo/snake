var SnakeUI;

(function () {
	var events = {
		left:  null, up: null, right: null, down: null,
		r: null		
	};

	var field,
			table;

	function _drawMap(map) {
		if (!map)
			return;

		var tr, td;

		table = $('<table></table>').css({
			width         : '100%',
			height        : '100%',
			border        : 'none',
			borderSpacing : '1px'
		});

		for (var i = 0, li = map.length; i<li; ++i) {
			tr = $('<tr></tr>').addClass('sn-r-' + i).appendTo(table);
			for (var j = 0, lj = map[i].length; j<lj; ++j) {
				td = $('<td></td>').addClass('sn-c-' + j)
				if (map[i][j])
					td.css({
						backgroundColor: 'black'
					});
				td.appendTo(tr);
			}
		}

		table.appendTo(field);
	}

	function _bindEvents() {
		$(document).keydown(function(e) {//only in events var
				switch(e.which) {
						case 37:
							trigger('left');
						break;

						case 38:
							trigger('up');
						break;

						case 39:
							trigger('right');
						break;

						case 40:
							trigger('down');
						break;

						case 82:
							trigger('r');
						break;

						default:
							return;
				}
				e.preventDefault();
		});
	}

	function trigger(eventName) {
		if (events[eventName])
			events[eventName]();
	}

	function on(eventName, callback) {
		if (
			callback
			&& callback instanceof Function
			&& events.hasOwnProperty(eventName)
		)
			events[eventName] = callback;
	}


	var lastSnakeXYs;

	function drawSnake(XYs) {
		if (!table)
			return;

		if(lastSnakeXYs)
			styleTableXYs(lastSnakeXYs, {backgroundColor: ''});

		styleTableXYs(XYs);
		lastSnakeXYs = XYs;
	}


	function styleTableXYs(XYs, cssObj) {
		if (!table)
			return;

		var css;

		for (var i = 0, l = XYs.length; i<l; ++i) {
			css = cssObj
				? cssObj
				: {backgroundColor: 'rgb(0,' + Math.ceil(255 - i/l * 255) + ', 0)'}

			table.find('.sn-r-' + XYs[i].y + ' ' + '.sn-c-' + XYs[i].x).css(css);
		}
	}

	function gameOver() {
		alert('Game Over!');
	}

	var lastFoodXYs;
	function drawFood(XYs) {
		if (!table)
			return;

		if(lastFoodXYs)
			styleTableXYs(lastFoodXYs, {backgroundColor: ''});

		styleTableXYs(XYs, {backgroundColor: 'red'});
		lastFoodXYs = XYs;
	}
	
	function eat(score) {
		$('#score').text(score);
	}

	SnakeUI = function (map) {
		$(document).ready(function () {
			field = $('#snake-field');

			_bindEvents();
			_drawMap(map);
		});

		this.drawSnake = drawSnake;
		this.drawFood  = drawFood;
		this.on        = on;
		this.gameOver  = gameOver;
		this.eat       = eat;
	}
})();

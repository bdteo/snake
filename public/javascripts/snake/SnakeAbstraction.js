var SnakeAbstraction;

(function () {
	var directions = {south: 'south', west: 'west', north: 'north', east: 'east'},
			events = {move: null, gameOver: null, foodGenerated: null, eat: null};

	function directionToXY(direction) {
		var hx = 0, hy = 0;

		if (direction === directions.south) ++hy;
		if (direction === directions.west)  --hx;
		if (direction === directions.north) --hy;
		if (direction === directions.east)  ++hx;

		return {
			x: hx,
			y: hy
		};
	}

	function Atom() {
		var x, y, direction;

		function setDirection(dir) {
			if (!directions[dir])
				return;

			if (dir === directions.south && direction === directions.north) return;
			if (dir === directions.north && direction === directions.south) return;
			if (dir === directions.east && direction === directions.west) return;
			if (dir === directions.west && direction === directions.east) return;

			direction = directions[dir];
		}

		function setX(val)   {x = val;}
		function setY(val)   {y = val;}


		function getDirection() {
			return direction;
		}
		function getX()   {return x;}
		function getY()   {return y;}

		function move(dir) {
			var change = directionToXY(dir);
			x += change.x;
			y += change.y;
		}

		function getXY() {
			return {
				x: x,
				y: y
			};
		}

		this.setDirection = setDirection;
		this.getDirection = getDirection;

		this.getX         = getX;
		this.getY         = getY;

		this.setX         = setX;
		this.setY         = setY;

		this.getXY        = getXY;
		this.move         = move;
	}

	function MultiAtom(x, y, dir) {
		var firstAtom = new Atom();
		firstAtom.setX(x);
		firstAtom.setY(y);
		firstAtom.setDirection(dir);

		var atoms = [firstAtom],
				top   = firstAtom;


		function augment() {
			var overTheTop = new Atom();

			var topDirection = top.getDirection(),
					change = directionToXY(topDirection);

			overTheTop.setDirection(topDirection);
			overTheTop.setX(top.getX() + change.x);
			overTheTop.setY(top.getY() + change.y);

			top = overTheTop;
			atoms.push(overTheTop);
		}

		function setDirection(dir) {
			return top.setDirection(dir);
		}

		function getDirection() {
			return top.getDirection();
		}

		function move(map) {
			if (!map)
				return;

			augment();// try to move on head
			var collisionType = detectCollision(top.getX(), top.getY(), map, getXYs());
			if (collisionType === 1) {
				atoms.pop();// rollback head
				return 1;
			}


			atoms.shift();// move on tail

			if (collisionType === 2) {
				eat(top.getX(), top.getY(), map, getXYs());
				augment();//enlarge snake
				return 2;
			}
		}


		function getXYs() {
			return atoms.map(function (val) {return val.getXY()});
		}

		this.augment      = augment;
		this.move      		= move;
		this.setDirection = setDirection;
		this.getDirection = getDirection;
		this.getXYs       = getXYs;
	}

	function detectCollision(x, y, map, snakeBodyXYs) {
		if (x < 0) return 1;
		if (y < 0) return 1;

		if (x > map[0].length - 1) return 1;
		if (y > map.length - 1)    return 1;

		if (map[y][x] === 1)
			return 1;

		if (map[y][x] === 2)
			return 2;

		var selfOverlaps = false;
		for (var i = 0, l = snakeBodyXYs.length; i<l-1; ++i) {
			if (snakeBodyXYs[i].x === x && snakeBodyXYs[i].y === y) {
				selfOverlaps = true;
				break;
			}
		}

		if (selfOverlaps)
			return 1;

		return 0;
	}

	function generateFood(map, snakeBodyXYs) {
		var w, h,
				rx, ry,
				overBody;

		do {
			w = map[0].length - 1;
			h = map.length - 1;
			rx = Math.floor(Math.random() * w);
			ry = Math.floor(Math.random() * h);

			overBody = false;
			for (var i = 0, l = snakeBodyXYs.length; i<l; ++i) {
				if (snakeBodyXYs[i].x === rx && snakeBodyXYs[i].y === ry) {
					overBody = true;
					break;
				}
			}
		}
		while (map[ry][rx] || overBody);

		map[ry][rx] = 2;

		var result = {
			x: rx,
			y: ry
		};

		return result;
	}



	var oldFoodXYs, foodCount;//used just to be able to expire old food easily

	function eat(x, y, map, snakeBodyXYs) {
		map[y][x] = 0;
		--foodCount;
		var size  = snakeBodyXYs.length,
				score = Math.pow(size, 4) + Math.pow(size, 3) + 3*Math.pow(size, 2) + Math.floor(Math.random() + size);
		trigger('eat', score);//score

		if (foodCount === 0)
			generateMultipleFood(map, snakeBodyXYs);
	};

	function generateMultipleFood(map, snakeBodyXYs) {
		if (oldFoodXYs) {
			for (var i = 0, l = oldFoodXYs.length; i<l; ++i)
				map[oldFoodXYs[i].y][oldFoodXYs[i].x] = 0;// remove old food
		}

		var newFood = [];

		foodCount = 2;
		for (var i = 0, l = foodCount; i<l; ++i) {
			newFood.push(generateFood(map, snakeBodyXYs));
		}

		oldFoodXYs = newFood;
		trigger('foodGenerated', newFood);
	}

	SnakeAbstraction = function (map) {
		var snakeBody;
		var moveInterval;
		var foodInterval;

		var moveTime = 150, foodTime = 15000;

		var moves;
		var started = false;

		function start() {
			trigger('eat', 0);//score 0
			moves = 0;
			
			snakeBody = new MultiAtom(0, 0, directions.south);
			generateMultipleFood(map, snakeBody.getXYs());
			
			started = true;
			setupMoveInterval(moveTime);			
			setupFoodInterval(foodTime);			

			var accelerateInterval = setInterval(function () {
				if (!started) {
					clearInterval(accelerateInterval);
					return;
				}

				var factor =  1 + Math.sqrt(moves) / 30;

				setupMoveInterval(Math.ceil(moveTime / factor));
			}, 4500);
		}
		
		
		function setupFoodInterval(interval) {
			clearInterval(foodInterval);
			foodInterval = setInterval(function () {generateMultipleFood(map, snakeBody.getXYs());}, interval);
		}

		function setupMoveInterval(interval) {
			clearInterval(moveInterval);			

			moveInterval = setInterval(function () {
				if (snakeBody.move(map) === 1) {
					started = false;
					clearInterval(moveInterval);
					clearInterval(foodInterval);

					trigger('gameOver');
					return;
				}

				++moves;
				trigger('move', snakeBody.getXYs());
			}, interval);
		}

		this.toSouth = function () {if (snakeBody) snakeBody.setDirection('south');}
		this.toWest  = function () {if (snakeBody) snakeBody.setDirection('west');}
		this.toNorth = function () {if (snakeBody) snakeBody.setDirection('north');}
		this.toEast  = function () {if (snakeBody) snakeBody.setDirection('east');}

		this.setFoodTime  = function (time) {foodTime = time;};
		this.setMoveTime  = function (time) {moveTime = time;};

		this.on    = on;
		this.start = start;
	}

	function on(eventName, callback) {
		if (
			callback
			&& callback instanceof Function
			&& events.hasOwnProperty(eventName)
		)
			events[eventName] = callback;
	}

	function trigger(eventName, eventData) {
		if (events[eventName])
			events[eventName](eventData);
	}
})();

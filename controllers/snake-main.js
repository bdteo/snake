//var Video = require('../models/user');

module.exports.controller = function(app) {

	/**
	* a main game page route
	*/
  app.route('/snake-main')
		.get(function(req, res) {
				var scriptDir = '/javascripts/snake/';
				
				res.render('snake-main', {
					title: 'Snake',
					scripts: [
						'//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js',
						scriptDir + 'SnakeAbstraction.js',
						scriptDir + 'SnakeUI.js',
						scriptDir + 'snakeMap.js',
						scriptDir + 'snakeInit.js'//todo: use requirejs
					]
				})
		});
}
